FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/zull-0.0.1-SNAPSHOT.jar zull.jar
ADD target/zull-0.0.1-SNAPSHOT.jar zull.jar
ENTRYPOINT ["java","-jar","/zull.jar"]